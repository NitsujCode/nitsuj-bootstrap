package com.initsuj.minecraft.plugins.bootstrap.command;

/**
 * Created by Nitsuj on 1/6/14.
 */

import com.initsuj.minecraft.plugins.bootstrap.BootStrap;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandMap;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.plugin.SimplePluginManager;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class CommandsHandler implements org.bukkit.command.CommandExecutor {

    private static Map<String, Executor> BootStrapCommands = new HashMap<String, Executor>();
    private static BootStrap Plugin;

    public CommandsHandler(BootStrap plugin) {
        Plugin = plugin;
    }

    public static Map<String, Executor> getBootStrapCommands() {
        return BootStrapCommands;
    }

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        CommandPackage commandPackage = new CommandPackage(sender, command, label, args);

        Plugin.getLogger().info("Executor caught: " + commandPackage.getCommand().getName());


        Executor executor = getBootStrapCommands().get(command.getName());
        return executor.execute(commandPackage);
    }

    public void addCommand(Executor executor) {
        BootStrapCommands.put(executor.getName(), executor);
        Plugin.getCommand(executor.getName()).setExecutor(this);
    }

    public void addCommand(DynamicExecutor bootStrapDynamicCommand) {
        BootStrapCommands.put(bootStrapDynamicCommand.getName(), bootStrapDynamicCommand);

        PluginCommand pluginCommand = getBukkitCommand(bootStrapDynamicCommand.getName());
        pluginCommand.setAliases(Arrays.asList(bootStrapDynamicCommand.getAliases()));
        pluginCommand.setDescription(bootStrapDynamicCommand.getDescription());
        pluginCommand.setPermission(bootStrapDynamicCommand.getPermission());
        pluginCommand.setPermissionMessage(bootStrapDynamicCommand.getPermissionMessage());
        pluginCommand.setUsage(bootStrapDynamicCommand.getCommandUsage());
        pluginCommand.setExecutor(this);
        getBukkitCommandMap().register(Plugin.getDescription().getName(), pluginCommand);
    }

    private static PluginCommand getBukkitCommand(String name) {
     /* code modified from:
        forums.bukkit.org
        user: ELCHILEN0 (http://forums.bukkit.org/members/elchilen0.90636997/)
        Title: [Tutorial] Registering commands at runtime!
        url: http://forums.bukkit.org/threads/tutorial-registering-commands-at-runtime.158461/
     */
        PluginCommand bukkitCommand = null;

        try {
            Constructor<PluginCommand> constructor = PluginCommand.class.getDeclaredConstructor(String.class, org.bukkit.plugin.Plugin.class);
            constructor.setAccessible(true);

            bukkitCommand = constructor.newInstance(name, Plugin);
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

        return bukkitCommand;
    }

    private static CommandMap getBukkitCommandMap() {

     /* code modified from:
        forums.bukkit.org
        user: ELCHILEN0 (http://forums.bukkit.org/members/elchilen0.90636997/)
        Title: [Tutorial] Registering commands at runtime!
        url: http://forums.bukkit.org/threads/tutorial-registering-commands-at-runtime.158461/
     */

        CommandMap commandMap = null;

        try {
            if (Bukkit.getPluginManager() instanceof SimplePluginManager) {
                Field field = SimplePluginManager.class.getDeclaredField("commandMap");
                field.setAccessible(true);

                commandMap = (CommandMap) field.get(Bukkit.getPluginManager());
            }
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return commandMap;
    }

    private boolean isPlayerCommand(org.bukkit.command.Command cmd) {
        return cmd.getName().equalsIgnoreCase("basic");
    }


}
