package com.initsuj.minecraft.plugins.bootstrap.chat;

import com.initsuj.minecraft.plugins.bootstrap.functions.FString;

/**
 * Created by Nitsuj on 1/8/14.
 */
public class Message {

    private String Message;

    public Message(String[] words) {
        this.Message = FString.getString(words);
    }

    public Message(String message) {
        this.Message = message;
    }

    private String FormattedString() {
        return this.Message.replaceAll("(?i)&([a-k0-9])", "\u00A7$1"); //replace '&' with minecraft format code character
    }

    public String toString() {
        return this.FormattedString();
    }

    public boolean isNullOrEmpty() {
        return FString.isNullOrEmpty(this.Message);
    }
}
