package com.initsuj.minecraft.plugins.bootstrap.chat.colors;

/**
 * Created by Nitsuj on 1/7/14.
 */

import org.bukkit.ChatColor;

import java.util.HashMap;
import java.util.Map;

/**
 * §0 	Black
 * §1 	Dark Blue
 * §2 	Dark Green
 * §3 	Dark Aqua
 * §4 	Dark Red
 * §5 	Dark Purple
 * §6 	Gold
 * §7 	Gray
 * §8 	Dark
 * §9 	Blue
 * §a 	Green
 * §b 	Aqua
 * §c 	Red
 * §d 	Light Purple
 * §e 	Yellow
 * §f 	White
 */

public class Colors {
    private static Map<String, ChatColor> ChatColorMap = initColorMap();

    private static Map<String, ChatColor> initColorMap() {
        Map<String, ChatColor> map = new HashMap<String, ChatColor>();

        map.put("aqua", ChatColor.AQUA);
        map.put("black", ChatColor.BLACK);
        map.put("blue", ChatColor.BLUE);
        map.put("b", ChatColor.BOLD);
        map.put("daqua", ChatColor.DARK_AQUA);
        map.put("dblue", ChatColor.DARK_BLUE);
        map.put("dgray", ChatColor.DARK_GRAY);
        map.put("dgreen", ChatColor.DARK_GREEN);
        map.put("dpurple", ChatColor.DARK_PURPLE);
        map.put("dred", ChatColor.DARK_RED);
        map.put("gold", ChatColor.GOLD);
        map.put("gray", ChatColor.GRAY);
        map.put("green", ChatColor.GREEN);
        map.put("i", ChatColor.ITALIC);
        map.put("lpurple", ChatColor.LIGHT_PURPLE);
        map.put("m", ChatColor.MAGIC);
        map.put("red", ChatColor.RED);
        map.put("r", ChatColor.RESET);
        map.put("s", ChatColor.STRIKETHROUGH);
        map.put("u", ChatColor.UNDERLINE);
        map.put("white", ChatColor.WHITE);
        map.put("yellow", ChatColor.YELLOW);

        return map;
    }

    public static ChatColor getChatColor(String color) {
        return ChatColorMap.get(color);
    }
}
