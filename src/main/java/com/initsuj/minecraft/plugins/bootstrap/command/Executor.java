package com.initsuj.minecraft.plugins.bootstrap.command;

/**
 * Created by Nitsuj on 1/6/14.
 */

public interface Executor {

    String getName();

    public boolean execute(CommandPackage commandPackage);

}
