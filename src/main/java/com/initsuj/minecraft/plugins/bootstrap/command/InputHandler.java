package com.initsuj.minecraft.plugins.bootstrap.command;

import com.initsuj.minecraft.plugins.bootstrap.functions.FString;

import java.util.ArrayList;

/**
 * Created by Nitsuj on 1/10/14.
 */

public class InputHandler {

    private ArrayList<Input> Inputs = new ArrayList<Input>();

    private Input SubCommand = new Input("");
    private Input SubCommandArg = new Input("");
    private Input CommandArgs = new Input("");
    private ArrayList<String> ArgList = new ArrayList<String>();

    public InputHandler(String[] inputs) {
        construct(inputs);
    }

    public boolean hasSubCommand() {
        return !FString.isNullOrEmpty(SubCommand.toString());
    }

    public boolean hasSubCommandArg() {
        return !FString.isNullOrEmpty(SubCommandArg.toString());
    }

    public boolean hasArgs() {
        return !FString.isNullOrEmpty(CommandArgs.toString());
    }

    private void setSubCommand(Input input) {
        this.SubCommand = input;
    }

    public Input get() {
        return this.SubCommand;
    }

    private void setSubCommandArg(Input input) {
        this.SubCommandArg = input;
    }

    public Input getSubCommandArg() {
        return this.SubCommandArg;
    }

    public Input getSubCommand() {
        return SubCommand;
    }

    private void addArg(String input) {
        this.ArgList.add(input);
    }

    public Input getArgs() {
        return this.CommandArgs;
    }

    private void setCommandArgs(Input input) {
        this.CommandArgs = input;
    }

    private void construct(String[] args) {

        if (args.length == 0) {
            return;
        }

        int i = 1;
        for (String arg : args) {

            Input input = new Input(arg);

            Inputs.add(input);

            switch (i) {
                case 1:
                    input.clean();
                    setSubCommand(input);
                    break;
                case 2:
                    input.clean();
                    setSubCommandArg(input);
                    addArg(arg);

                    break;
                default:
                    addArg(arg);
            }

            i++;
        }

        setCommandArgs(new Input(FString.getString(this.ArgList)));

    }
}
