package com.initsuj.minecraft.plugins.bootstrap;

/**
 * Created by Nitsuj on 1/6/14.
 */


import com.initsuj.minecraft.plugins.bootstrap.command.CommandsHandler;
import org.bukkit.plugin.java.JavaPlugin;

public class BootStrap extends JavaPlugin {

    public com.initsuj.minecraft.plugins.bootstrap.command.CommandsHandler CommandsHandler = new CommandsHandler(this);
}
