package com.initsuj.minecraft.plugins.bootstrap.functions;

import java.util.ArrayList;

/**
 * Created by Nitsuj on 1/8/14.
 */

public class FString {

    private static String Filler = " ";

    public static String getString(String[] stringArray) {

        StringBuilder builder = new StringBuilder();
        for (String s : stringArray) {
            builder.append(s);
            builder.append(Filler);
        }

        resetFillerToDefault(); //reset Filler to default for next use.

        return builder.toString();
    }

    public static String getString(String[] stringArray, String filler) {
        Filler = filler;
        return getString(stringArray);
    }

    public static String getString(ArrayList<String> list) {
        StringBuilder builder = new StringBuilder();
        for (String s : list) {
            builder.append(s);
            builder.append(Filler);
        }

        resetFillerToDefault(); //reset Filler to default for next use.

        return builder.toString();
    }

    public static String getString(ArrayList<String> list, String filler) {
        Filler = filler;
        return getString(list);
    }

    public static String removeNonCharacters(String str) {
        return str.replaceAll("\\s", "");
    }

    public static boolean isNumeric(String str) {
        for (char c : str.toCharArray()) {
            if (!Character.isDigit(c)) return false;
        }
        return true;
    }

    public static int toInt(String str) {
        return Integer.parseInt(str);
    }

    public static boolean isNullOrEmpty(String str) {
        try {
            if (str == null || str.isEmpty()) {
                return true;
            }
        } catch (Exception exception) {
            return false;
        }

        return false;
    }

    private static void resetFillerToDefault() {
        Filler = " ";
    }

}
