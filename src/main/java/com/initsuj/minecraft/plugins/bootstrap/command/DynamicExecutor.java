package com.initsuj.minecraft.plugins.bootstrap.command;

/**
 * Created by Nitsuj on 1/9/14.
 */

public interface DynamicExecutor extends Executor {
    String[] getAliases();

    String getDescription();

    String getPermission();

    String getPermissionMessage();

    String getCommandUsage();
}
