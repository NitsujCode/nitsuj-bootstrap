package com.initsuj.minecraft.plugins.bootstrap.command;

import com.initsuj.minecraft.plugins.bootstrap.functions.FString;

/**
 * Created by Nitsuj on 1/10/14.
 */

public class Input {

    private String Arg;

    public Input(String arg) {
        this.Arg = arg;
    }

    public boolean isNumeric() {
        return FString.isNumeric(Arg);
    }

    public int toInt() {
        return FString.toInt(Arg);
    }

    public String toString() {
        return Arg;
    }

    public void clean() {
        this.Arg = FString.removeNonCharacters(this.Arg);
    }
}
