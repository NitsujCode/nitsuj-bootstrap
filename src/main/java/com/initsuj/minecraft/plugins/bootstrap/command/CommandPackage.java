package com.initsuj.minecraft.plugins.bootstrap.command;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

/**
 * Created by Nitsuj on 1/10/14.
 */

public class CommandPackage {
    private CommandSender CommandSender;
    private Command Command;
    private String Label;
    private InputHandler InputHandler;

    public CommandPackage(CommandSender commandSender, Command command, String label, String[] args) {
        this.CommandSender = commandSender;
        this.Command = command;
        this.Label = label;
        this.InputHandler = new InputHandler(args);
    }

    public CommandSender getCommandSender() {
        return this.CommandSender;
    }

    public Command getCommand() {
        return this.Command;
    }

    public String getLabel() {
        return this.Label;
    }

    public InputHandler getInputHandler() {
        return this.InputHandler;
    }
}
